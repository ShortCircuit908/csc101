import math

# Author: Caleb Milligan
# Created on 9/1/2016


def print_powers():
    nums = []
    widths = []
    for i in range(33):
        val = int(math.pow(2, i))
        nums.append(val)
        widths.append([len(str(val)), len(hex(val)), len(bin(val))])
    max_widths = [3, 3, 3]
    for width in widths:
        for i in range(3):
            if width[i] > max_widths[i]:
                max_widths[i] = width[i]
    cap_str = "+"
    for max_width in max_widths:
        cap_str += "-" + str.ljust("", max_width, '-') + "-+"
    print(cap_str)
    print("| " + "Dec".ljust(max_widths[0]) + " | "
          + "Hex".ljust(max_widths[1]) + " | "
          + "Bin".ljust(max_widths[2]) + " |")
    print(cap_str)
    for i in range(len(nums)):
        print("| " + str(nums[i]).ljust(max_widths[0]) + " | "
              + str(hex(nums[i])).ljust(max_widths[1]) + " | "
              + str(bin(nums[i]).ljust(max_widths[2])) + " |")
    print(cap_str)


def print_div_4():
    x = -1
    while x < 0 or x > 10000:
        try:
            x = int(input("Number from 0 to 10000: "))
        except ValueError:
            print("Please enter a valid number")
    print(math.floor(x / 4))

print_powers()
print_div_4()
