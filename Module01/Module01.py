import math

# Author: Caleb Milligan
# Created on 9/1/2016


def calc_wii():
    nintendo_wii_price = 299.99
    print("How much money do you have?")
    total_money = math.nan
    while math.isnan(total_money):
        try:
            total_money = float(input("> "))
        except ValueError:
            print("Please enter a number")
    print(str.format("You can afford {} Nintendo Wii system(s)", math.floor(total_money / nintendo_wii_price)))


def calc_triangle():
    w = -1
    h = -1
    while w < 0:
        try:
            w = float(input("Width of the base: "))
        except ValueError:
            print("Please input a number >= 0")
    while h < 0:
        try:
            h = float(input("Height of the triangle: "))
        except ValueError:
            print("Please input a number >= 0")
    print(str.format("The triangle is {} square units", 0.5 * w * h))


def calc_gcd():
    a = 0
    b = 0
    while a <= 0:
        try:
            a = int(input("a > 0: "))
        except ValueError:
            print("Please enter a number greater than 0")
    while b <= 0:
        try:
            b = int(input("b > 0: "))
        except ValueError:
            print("Please enter a number greater than 0")
    gcd = 1
    for i in range(1, min(a, b) + 10):
        if math.floor(a / i) == a / i and math.floor(b / i) == b / i:
            gcd = i
    print(str.format("The greatest common divisor of {} and {} is {}", a, b, gcd))


def calc_lcm():
    a = 0
    b = 0
    while a <= 0:
        try:
            a = int(input("a > 0: "))
        except ValueError:
            print("Please enter a number greater than 0")
    while b <= 0:
        try:
            b = int(input("b > 0: "))
        except ValueError:
            print("Please enter a number greater than 0")
    large = max(a, b)
    lcm = a * b
    while large < a * b:
        if (large % a == 0) and (large % b == 0):
            lcm = large
            break
        large += 1
    print(str.format("The least common multiple of {} and {} is {}", a, b, lcm))


def calc_x_plus_y_squared():
    x = math.nan
    y = math.nan
    while math.isnan(x):
        try:
            x = float(input("x: "))
        except ValueError:
            print("Please enter a number")
    while math.isnan(y):
        try:
            y = float(input("y: "))
        except ValueError:
            print("Please enter a number")
    print(str.format("({} + {})^2 = {}", x, y, math.pow(x + y, 2)))


def input_point(pnum):
    x = math.nan
    y = math.nan
    while math.isnan(x):
        try:
            x = float(input("x" + str(pnum) + ": "))
        except ValueError:
            print("Please enter a valid number")
    while math.isnan(y):
        try:
            y = float(input("y" + str(pnum) + ": "))
        except ValueError:
            print("Please enter a valid number")
    return {"x": x, "y": y}


def calc_distance():
    p1 = input_point(1)
    p2 = input_point(2)
    dist = math.sqrt(math.pow(p2["y"] - p1["y"], 2) + math.pow(p2["x"] - p1["x"], 2))
    print(str.format("The distance between {} and {} is {} units", p1, p2, dist))

calc_wii()
calc_triangle()
calc_gcd()
calc_lcm()
calc_x_plus_y_squared()
calc_distance()
