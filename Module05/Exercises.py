from graphics import *
from tkinter import Canvas
from turtle import *

"""
c = Canvas()
g = GraphWin("Title", 500, 500)


def squares():
    for i in range(10):
        rect = Rectangle(Point(g.width - (50 * (i + 1)), i * 50), Point(g.width, (i + 1) * 50))
        rect.setWidth(5)
        rect.draw(g)

squares()
g.getMouse()
g.close()
"""


def string_times(s: str, x: int):
    return s * x

# 2 * x
print(string_times("Hi", 2))


t = Turtle()
t.speed(0)
t.hideturtle()
for i in range(40):
    t.setheading(9 * i)
    t.forward(125)
    t.left(50)
    t.forward(125)
    t.left(130)
    t.forward(125)
    t.left(50)
    t.forward(125)
# 10 * 40
t.screen.mainloop()
