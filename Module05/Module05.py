# Author: Caleb Milligan
# Created on 10/4/2016

from graphics import *
import time
import math

g = GraphWin("Title", 500, 500, autoflush=False)


def triangle():
    tri = Polygon(Point(0, 0), Point(50, 0), Point(25, 50))
    tri.setFill("blue")
    tri.draw(g)
    g.update()
    time.sleep(1)


def animated_triangle():
    x = 0
    y = 0
    x_step = 1
    y_step = 1
    if g.width > g.height:
        y_step = g.height / g.width
    if g.width < g.height:
        x_step = g.width / g.height
    while x + 50 < g.width or y + 50 < g.height:
        g.delete("all")
        tri = Polygon(Point(x, y), Point(x + 50, y), Point(x + 25, y + 50))
        tri.setFill("blue")
        tri.draw(g)
        x += x_step
        y += y_step
        g.update()
        time.sleep(1 / 120)


def face(center: Point = Point(g.width / 2, g.height / 2)):
    obj = Circle(center, 100)
    obj.setFill("yellow")
    obj.draw(g)
    obj = Circle(Point(center.x - 40, center.y - 25), 20)
    obj.setFill("black")
    obj.draw(g)
    obj = Circle(Point(center.x + 40, center.y - 25), 20)
    obj.setFill("black")
    obj.draw(g)
    obj = Oval(Point(center.x - 65, center.y + 15), Point(center.x + 65, center.y + 80))
    obj.setFill("black")
    obj.draw(g)
    obj = Oval(Point(center.x - 65, center.y + 10), Point(center.x + 65, center.y + 55))
    obj.setOutline("yellow")
    obj.setFill("yellow")
    obj.draw(g)
    g.update()


def animated_face():
    center = Point(g.width / 2, g.height / 2)
    for i in range(360):
        g.delete("all")
        angle = math.radians(i)
        x = math.cos(angle) * 100
        y = math.sin(angle) * 100
        face(Point(center.x + x, center.y + y))
        time.sleep(1 / 120)


def main():
    triangle()
    g.delete("all")
    g.update()
    time.sleep(0.5)
    animated_triangle()
    g.delete("all")
    g.update()
    time.sleep(0.5)
    face()
    time.sleep(1)
    g.delete("all")
    g.update()
    time.sleep(0.5)
    animated_face()

main()
g.getMouse()
g.close()
