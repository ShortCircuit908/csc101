from turtle import *
from graphics import *


def draw_sierpinski(level: int, g: GraphWin or Turtle, p1x: int, p1y: int, p2x: int, p2y: int, p3x: int, p3y: int):
    if level <= 1:
        if isinstance(g,Turtle):
            g.penup()
            g.goto(p1x, p1y)
            g.pendown()
            g.begin_fill()
            g.goto(p2x, p2y)
            g.goto(p3x, p3y)
            g.goto(p1x, p1y)
            g.end_fill()
            g.penup()
        elif isinstance(g, GraphWin):
            p = Polygon(Point(p1x, p1y), Point(p2x, p2y), Point(p3x, p3y))
            p.setFill("lime")
            p.setOutline("blue")
            p.draw(g)
        return
    p4x = (p1x + p2x) / 2
    p4y = (p1y + p2y) / 2
    p5x = (p2x + p3x) / 2
    p5y = (p2y + p3y) / 2
    p6x = (p3x + p1x) / 2
    p6y = (p3y + p1y) / 2
    draw_sierpinski(level - 1, g, p1x, p1y, p4x, p4y, p6x, p6y)
    draw_sierpinski(level - 1, g, p4x, p4y, p2x, p2y, p5x, p5y)
    draw_sierpinski(level - 1, g, p6x, p6y, p5x, p5y, p3x, p3y)


def string_match(s1: str, s2: str):
    count = 0
    for i in range(min(len(s1), len(s2)) - 1):
        if s1[i:i+2] == s2[i:i+2]:
            count += 1
    return count


def sierpinski_with_turtle():
    t = Turtle()
    t.hideturtle()
    t.speed(0)
    t.color("blue")
    t.fillcolor("lime")
    return t


def sierpinski_with_graphics():
    g = GraphWin("Sierpinski", 650, 650, autoflush=False)
    g.setCoords(-325, -325, 325, 325)
    return g


def main():
    print(string_match('xxcaazz', 'xxbaaz'))
    print(string_match('abc', 'abc'))
    print(string_match('abc', 'axc'))

    g = sierpinski_with_graphics()
    draw_sierpinski(4, g, -200, -200, 0, 200, 200, -200)
    if isinstance(g, GraphWin):
        g.getMouse()
        g.close()

main()
