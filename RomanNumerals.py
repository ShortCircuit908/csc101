def to_roman_numerals(num: int):
    romanized = "" if num >= 0 else "-"
    num = abs(int(num))
    vals = [
        [1000, "M"],
        [900, "CM"],
        [500, "D"],
        [400, "CD"],
        [100, "C"],
        [90, "XC"],
        [50, "L"],
        [40, "XL"],
        [10, "X"],
        [9, "IX"],
        [5, "V"],
        [4, "IV"],
        [1, "I"]
    ]
    while num > 0:
        for val in vals:
            if num - val[0] >= 0:
                romanized += val[1]
                num -= val[0]
                break
    return romanized


def from_roman_numerals(string: str):
    sign = 1
    orig = string = string.upper()
    while string.startswith("+") or string.startswith("-"):
        if string.startswith("+"):
            string = string[1:]
        elif string.startswith("-"):
            sign *= -1
            string = string[1:]
    decimal = 0
    vals = [
        [900, "CM"],
        [1000, "M"],
        [400, "CD"],
        [500, "D"],
        [90, "XC"],
        [100, "C"],
        [40, "XL"],
        [50, "L"],
        [9, "IX"],
        [10, "X"],
        [4, "IV"],
        [5, "V"],
        [1, "I"]
    ]
    while len(string) > 0:
        new_string = string
        try:
            for val in vals:
                new_string = string.replace(val[1], "", 1)
                if new_string != string:
                    decimal += val[0]
                    string = new_string
                    raise StopIteration
        except StopIteration:
            continue
        if new_string == string and len(string) > 0:
            raise ValueError("Cannot parse string \"{}\": invalid characters: {}".format(orig, list(string)))
    return decimal * sign

x = to_roman_numerals(1998)
print(x)
print(from_roman_numerals(x))


def rot_n_dict(n: int):
    keys = list("\r\n\t !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~")
    values = [None] * len(keys)
    for i in range(len(keys)):
        values[(i + n) % len(keys)] = keys[i]
    return dict(zip(keys, values))


def rot_decode(encoded: str, n: int = 13):
    table = rot_n_dict(n)
    decoded = ""
    for ch in encoded:
        decoded += table[ch] if ch in table else ch
    return decoded


def rot_encode(decoded: str, n: int = 13):
    table = rot_n_dict(n)
    encoded = ""
    for ch in decoded:
        found = False
        for key in table:
            if table[key] == ch:
                encoded += key
                found = True
                break
        if not found:
            encoded += ch
    return encoded

from io import *
file = open(__file__, "r", encoding="utf-8")
decoded = ""
for line in file:
    decoded += line
file.close()
print("Performing rotation cipher on this source file")
num = int(input("Positions to rotate: "))
encoded = rot_encode(decoded, num)
decoded = rot_decode(encoded, num)
print("###########\n# ENCODED #\n###########\n\n{}\n\n###########\n# DECODED #\n###########\n\n{}".format(encoded, decoded))
