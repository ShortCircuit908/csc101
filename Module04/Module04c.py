# Author: Caleb Milligan
# Created on 9/27/2016
import random
import functools

def calc_grades(students: dict):
    higher_fourth = []
    avg = 0
    minimum = None
    maximum = None
    for grade in students.values():
        avg += grade
        if minimum is None or grade < minimum:
            minimum = grade
        if maximum is None or grade > maximum:
            maximum = grade
    avg /= len(students)
    above_avg = 0
    below_avg = 0

    sorted_keys = sorted(students, key=lambda x: [1])
    index = round(0.75 * len(students))
    for i in range(index, len(students)):
        higher_fourth.append(sorted_keys[i])
    for name, grade in students.items():
        if grade > avg:
            above_avg += 1
        elif grade < avg:
            below_avg += 1

    pcnt_above_avg = round((above_avg / len(students)) * 100, 1)
    pcnt_below_avg = round((below_avg / len(students)) * 100, 1)

    print("Mean score: {}".format(avg))
    print("Max score: {}".format(maximum))
    print("Min score: {}".format(minimum))
    print("Percent above mean: {}%".format(pcnt_above_avg))
    print("Percent below mean: {}%".format(pcnt_below_avg))
    print("Students in higher 4th percentile: {}".format(", ".join(higher_fourth)))


def traveling_salesman():
    class Ticket:
        start_location = None
        destination = None

        def __init__(self, start_location, destination):
            self.start_location = start_location
            self.destination = destination

        def __str__(self):
            return "{} to {}".format(self.start_location, self.destination)

        def __repr__(self):
            return self.__str__()

    cities = [
        "Aberdeen",
        "Airway Heights",
        "Algona",
        "Anacortes",
        "Arlington",
        "Asotin",
        "Auburn",
        "Pierce",
        "Bainbridge Island",
        "Battle Ground",
        "Bellevue",
        "Bellingham",
        "Benton City",
        "Bingen",
        "Black Diamond",
        "Blaine",
        "Bonney Lake",
        "Bothell",
        "Snohomish",
        "Bremerton",
        "Brewster",
        "Bridgeport",
        "Brier",
        "Buckley",
        "Burien",
        "Burlington",
        "Camas",
        "Carnation",
        "Cashmere",
        "Castle Rock",
        "Centralia",
        "Chehalis",
        "Chelan",
        "Cheney",
        "Chewelah",
        "Clarkston",
        "Cle Elum",
        "Clyde Hill",
        "Colfax",
        "College Place",
        "Colville",
        "Connell",
        "Cosmopolis",
        "Covington",
        "Davenport",
        "Dayton",
        "Deer Park",
        "Des Moines",
        "DuPont",
        "Duvall",
        "East Wenatchee",
        "Edgewood",
        "Edmonds",
        "Electric City",
        "Ellensburg",
        "Elma",
        "Entiat",
        "Enumclaw",
        "Pierce",
        "Ephrata",
        "Everett",
        "Everson",
        "Federal Way",
        "Ferndale",
        "Fife",
        "Fircrest",
        "Forks",
        "George",
        "Gig Harbor",
        "Gold Bar",
        "Goldendale",
        "Grand Coulee",
        "Grandview",
        "Granger",
        "Granite Falls",
        "Harrington",
        "Hoquiam",
        "Ilwaco",
        "Issaquah",
        "Kahlotus",
        "Kalama",
        "Kelso",
        "Kenmore",
        "Kennewick",
        "Kent",
        "Kettle Falls",
        "Kirkland",
        "Kittitas",
        "La Center",
        "Lacey",
        "Lake Forest Park",
        "Lake Stevens",
        "Lakewood",
        "Langley",
        "Leavenworth",
        "Liberty Lake",
        "Long Beach",
        "Longview",
        "Lynden",
        "Lynnwood",
        "Mabton",
        "Maple Valley",
        "Marysville",
        "Mattawa",
        "McCleary",
        "Medical Lake",
        "Medina",
        "Mercer Island",
        "Mesa",
        "Mill Creek",
        "Millwood",
        "Milton",
        "King",
        "Monroe",
        "Montesano",
        "Morton",
        "Moses Lake",
        "Mossyrock",
        "Mount Vernon",
        "Mountlake Terrace",
        "Moxee",
        "Mukilteo",
        "Napavine",
        "Newcastle",
        "Newport",
        "Nooksack",
        "Normandy Park",
        "North Bend",
        "North Bonneville",
        "Oak Harbor",
        "Oakville",
        "Ocean Shores",
        "Okanogan",
        "Olympia",
        "Omak",
        "Oroville",
        "Orting",
        "Othello",
        "Pacific",
        "Pierce",
        "Palouse",
        "Pasco",
        "Pateros",
        "Pomeroy",
        "Port Angeles",
        "Port Orchard",
        "Port Townsend",
        "Poulsbo",
        "Prescott",
        "Prosser",
        "Pullman",
        "Puyallup",
        "Quincy",
        "Rainier",
        "Raymond",
        "Redmond",
        "Renton",
        "Republic",
        "Richland",
        "Ridgefield",
        "Ritzville",
        "Rock Island",
        "Roslyn",
        "Roy",
        "Royal City",
        "Ruston",
        "Sammamish",
        "SeaTac",
        "Seattle",
        "Sedro",
        "Selah",
        "Sequim",
        "Shelton",
        "Shoreline",
        "Snohomish",
        "Snoqualmie",
        "Soap Lake",
        "South Bend",
        "Spangle",
        "Spokane",
        "Spokane Valley",
        "Sprague",
        "Stanwood",
        "Stevenson",
        "Sultan",
        "Sumas",
        "Sumner",
        "Sunnyside",
        "Tacoma",
        "Tekoa",
        "Tenino",
        "Tieton",
        "Toledo",
        "Tonasket",
        "Toppenish",
        "Tukwila",
        "Tumwater",
        "Union Gap",
        "University Place",
        "Vader",
        "Vancouver",
        "Waitsburg",
        "Walla Walla",
        "Wapato",
        "Warden",
        "Washougal",
        "Wenatchee",
        "West Richland",
        "Westport",
        "White Salmon",
        "Winlock",
        "Woodinville",
        "Woodland",
        "Clark",
        "Woodway",
        "Yakima",
        "Yelm",
        "Zillah"
    ]
    tickets = []
    cur_pos = cities[0]
    next_pos = cities[0]
    for i in range(50):
        while next_pos == cur_pos:
            next_pos = cities[random.randint(0, len(cities) - 1)]
        tickets.append(Ticket(cur_pos, next_pos))
        cur_pos = next_pos
    random.shuffle(tickets)

    chain = join_tickets(tickets, 0)
    for ticket in chain:
        print(ticket)


def join_tickets(tickets: list, start_index: int):
    clone = tickets[:]
    chain = [clone[start_index]]
    del clone[start_index]
    for i in range(len(clone)):
        for j in range(len(clone)):
            if clone[j].start_location == chain[len(chain) - 1].destination:
                chain.append(clone[j])
                del clone[j]
                break
    # We've put the whole list in order!
    if len(clone) == 0:
        return chain
    # We've tried every element as the start of the chain. Reshuffle and try again.
    if start_index == len(tickets) - 1:
        random.shuffle(tickets)
        return join_tickets(tickets, 0)
    # Try again, using the next element as the start of the chain
    return join_tickets(tickets, start_index + 1)


def main():
    # I chose to use a dictionary because it maps keys to values,
    # which is good here for mapping names to scores
    students = {}
    for i in range(500):
        students["Student #" + str(i)] = random.randint(0, 100)
    calc_grades(students)
    print()

    # I chose to use a list because it can represent an ordered set
    # of elements, such as a chain of travel tickets
    traveling_salesman()

main()
