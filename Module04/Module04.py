# Author: Caleb Milligan
# Created on 9/22/2016


def ingly(string: str):
    if len(string) < 3:
        return string
    if string.endswith("ing"):
        return string + "ly"
    return string + "ing"


def print_removed(vals: list):
    del vals[0], vals[3], vals[3]
    print(vals)


def squares_dict():
    squares = {}
    for i in range(1, 16):
        squares[i] = i ** 2
    print(squares)


def main():
    # Part 1
    print(ingly("abc"))
    print(ingly("string"))
    # Part 2
    print_removed(["Red", "Green", "White", "Black", "Pink", "Yellow"])
    # Part 3
    squares_dict()

main()
