# Author: Caleb Milligan
# Created on 9/27/2016

from collections import deque


# 1:
def list_as_stack():
    stack = []
    stack.append(4)
    stack.append(9)
    stack.pop()
    stack.append(12)


# 2:
def list_as_queue():
    queue = deque()
    queue.append(4)
    queue.append(9)
    queue.popleft()
    queue.append(12)


# 3: 1, 0 (C)

# 4: [99, 100, None, 113, 114, None, 116, 117, 105, 97, 108] (B)
def linear_probing(keys: list, table_size: int):
    table = [None] * table_size
    for key in keys:
        vhash = key % len(table)
        if table[vhash] is None:
            table[vhash] = key
            continue
        for i in range(1, len(table)):
            slot = (vhash + i) % len(table)
            if table[slot] is None:
                table[slot] = key
                break
    return table

print(linear_probing([113, 117, 97, 100, 114, 108, 116, 105, 99], 11))
