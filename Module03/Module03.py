import re
import math

# Author: Caleb Milligan
# Created on 9/6/2016


def is_palindrome(string: str):
    new_string = re.sub(r'\s', "", string.lower())
    return new_string == new_string[::-1], string


def is_perfect(num: int):
    total = 0
    for i in range(1, num):
        if num % i == 0:
            total += i
    return total == num, num


def find_max(a, b, c):
    if a >= b and a >= c:
        return a, [a, b, c]
    if b >= a and b >= c:
        return b, [a, b, c]
    return c, [a, b, c]


def calc_sum(values: list):
    total = 0
    for val in values:
        total += val
    return total, values


def calc_mult(values: list):
    result = None
    for value in values:
        if result is None:
            result = value
        else:
            result *= value
    return result, values


def reverse(string: str):
    return string[::-1], string


def calc_factorial(num: int):
    factorial = num
    for i in range(num - 1, 0, -1):
        factorial *= i
    return factorial, num


def in_range(num, minimum, maximum):
    return minimum <= num <= maximum, num, minimum, maximum


def num_upper_lower(string: str):
    chars = list(string)
    upper = 0
    lower = 0
    for char in chars:
        if str.isalpha(char):
            if str.isupper(char):
                upper += 1
            else:
                lower += 1
    return lower, upper, string


def unique(values: list):
    result = []
    for value in values:
        if result.count(value) == 0:
            result.append(value)
    return result, values


def is_prime(num: int):
    for i in range(2, math.floor(num / 2)):
        if num % i == 0:
            return False, num
    return True, num


def find_evens(values: list):
    result = []
    for value in values:
        if value % 2 == 0:
            result.append(value)
    return result, values


def pascal(rows: int):
    print("The first {} rows of Pascal's Triangle are:".format(rows))
    row = None
    for i in range(rows):
        row = calc_row(row, i)
        print(row)


def calc_row(orig: list, n: int):
    result = [1]
    if n == 0:
        return result
    for i in range(len(orig) - 1):
        x = orig[i] + orig[i + 1]
        result.append(int(x))
    result.append(1)
    return result


def is_pangram(string: str):
    letters = list("abcdefghijklmnopqrstuvwxyz")
    new_string = string.lower()
    for letter in letters:
        if new_string.count(letter) == 0:
            return False
    return True, string


def sort_hyphen(string: str):
    words = string.split("-")
    words.sort()
    return "-".join(words), string


def squares():
    print("The squares of the numbers 1-30 are:")
    result = []
    for i in range(1, 31):
        result.append(i * i)
    print(result)


def italics(func):
    def apply_style():
        return "<i>{}</i>".format(func())
    return apply_style


def underline(func):
    def apply_style():
        return "<u>{}</u>".format(func())
    return apply_style


def bold(func):
    def apply_style():
        return "<b>{}</b>".format(func())
    return apply_style


@italics
@underline
@bold
def style_decorators():
    return "This will have lots of styling"


def run_code(source: str):
    exec(source)

print("Is \"{0[1]}\" a palindrome? {0[0]}".format(is_palindrome("Rats live on no evil star")))
print("Is {0[1]} a perfect number? {0[0]}".format(is_perfect(28)))
print("The maximum of {0[1]} is {0[0]}".format(find_max(15, 52, 8)))
print("The sum of {0[1]} is {0[0]}".format(calc_sum([8, 2, 3, 0, 7])))
print("{0[1]} multiplied is {0[0]}".format(calc_mult([8, 2, 3, -1, 7])))
print("{0[1]} reversed is {0[0]}".format(reverse("1234abcd")))
print("{0[1]}! = {0[0]}".format(calc_factorial(9)))
print("Is {0[1]} within the range [{0[2]}, {0[3]}]? {0[0]}".format(in_range(4, 0, 5)))
print("\"{0[2]}\" contains {0[1]} uppercase and {0[0]} lowercase letters".format(num_upper_lower("The quick Brow Fox")))
print("The unique elements in {0[1]} are {0[0]}".format(unique([1, 2, 3, 3, 3, 3, 4, 5])))
print("Is {0[1]} a prime number? {0[0]}".format(is_prime(11)))
print("The even numbers in {0[1]} are {0[0]}".format(find_evens([1, 2, 3, 4, 5, 6, 7, 8, 9])))
pascal(5)
print("Is \"{0[1]}\" a pangram? {0[0]}".format(is_pangram("The quick brown fox jumps over the lazy dog")))
print("\"{0[1]}\" sorted is \"{0[0]}\"".format(sort_hyphen("green-red-yellow-black-white")))
squares()
print(style_decorators())
run_code("print(\"Hello, world!\")")
