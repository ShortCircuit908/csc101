import random


def sort(arr: list):
    heapify(arr)
    n = len(arr) - 1
    for i in range(n, 0, -1):
        swap(arr, 0, i)
        n -= 1
        maxheap(arr, 0, n)


def heapify(arr: list):
    n = len(arr) - 1
    for i in range(int(n / 2), -1, -1):
        maxheap(arr, i, n)


def maxheap(arr: list, i: int, n: int):
    left = i * 2
    right = i * 2 + 1
    max_v = i
    if left <= n and arr[left] > arr[i]:
        max_v = left
    if right <= n and arr[right] > arr[max_v]:
        max_v = right
    if max_v != i:
        swap(arr, i, max_v)
        maxheap(arr, max_v, n)
    return n


def swap(arr: list, i: int, j: int):
    tmp = arr[i]
    arr[i] = arr[j]
    arr[j] = tmp


def main():
    arr = []
    for i in range(32):
        arr.append(i)
    random.shuffle(arr)
    print(arr)
    sort(arr)
    print(arr)

main()
