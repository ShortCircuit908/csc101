from graphics import *
from threading import *
import time
import math
import random
from io import FileIO
from datetime import datetime
from typing import List


class Game:
    # Global variables
    high_score_filename = "hiscore.dat"
    DEBUG = True
    FRAMERATE = 1 / 60
    GAME_OBJECTS = []
    GAME_STATE = 0
    SCORE = 0
    LIVES = 3
    LEVEL = 1
    PAUSED = False
    PADDLE = None
    BALL = None
    BRICKS = []
    HIGH_SCORE = 0

    @staticmethod
    def load_high_score():
        try:
            file = FileIO(Game.high_score_filename, "r")
            Game.HIGH_SCORE = int(file.readall())
            file.close()
        except (ValueError, FileNotFoundError):
            pass

    @staticmethod
    def save_high_score():
        file = FileIO(Game.high_score_filename, "w")
        file.write(bytearray(str(Game.HIGH_SCORE), "UTF-8"))
        file.flush()
        file.close()

    @staticmethod
    def gen_bricks(g: GraphWin):
        colors = ["red", "orange", "yellow", "green", "blue", "purple"]
        # For each color, generate a row of 20 bricks and place them
        for y in range(len(colors)):
            for x in range(10):
                b = Brick(g)
                b.x = (x + 1) * (b.width / 11) + (x * b.width)
                b.y = (y + 1) * (b.width / 11) + (y * b.height)
                b.color = colors[y]
                Game.GAME_OBJECTS.append(b)
                Game.BRICKS.append(b)


# A timer which repeatedly runs its target
class RepeatingTimer:
    def __init__(self, target: callable, period: float, delay: float=0, daemon: bool=None, name=None, group=None, args=None, kwargs=None):
        self.target = target
        self.period = period
        self.delay = delay
        self.thread = Thread(target=self.run, daemon=daemon, name=name, group=group)
        self.cancelled = False
        self.args = args if args is not None else []
        self.kwargs = kwargs if kwargs is not None else {}

    def getName(self):
        return self.thread.getName()

    def setName(self, name: str):
        self.thread.setName(name)

    def cancel(self):
        self.cancelled = True

    def is_alive(self):
        return self.thread.is_alive()

    def isDaemon(self):
        return self.thread.isDaemon()

    def setDaemon(self, daemonic: bool):
        self.thread.setDaemon(daemonic)

    def join(self, timeout):
        self.thread.join(timeout)

    def start(self):
        self.thread.start()

    def run(self):
        if self.delay > 0:
            time.sleep(self.delay)
        while not self.cancelled:
            now = datetime.now()
            self.target(*self.args, **self.kwargs)
            elapsed = (datetime.now() - now).microseconds
            if elapsed < self.period:
                time.sleep(self.period - (elapsed * 0.000001))


# Base class for game objects
class Renderable:
    def __init__(self, x=0, y=0, width=None, height=None, color="white"):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.color = color
        if Game.DEBUG:
            print("Create object: " + str(self))

    # Default draw method
    def draw(self, g: GraphWin):
        if not self.width or not self.height:
            return
        # Draw a rectangle
        p1 = Point(self.x, self.y)
        p2 = Point(self.x + self.width, self.y + self.height)
        rect = Rectangle(p1, p2)
        rect.setOutline(self.color)
        rect.setFill(self.color)
        rect.draw(g)

    # Default update method, should be overridden
    def update(self, g: GraphWin, delta_t: float):
        pass

    # Get list of game objects touching the left side of this object
    def contact_left(self, accuracy=5):
        contacted = []
        # Iterate over all game objects
        for game_object in Game.GAME_OBJECTS:
            other_right = game_object.x + game_object.width
            # Lots of bounds checks
            if game_object != self \
                    and other_right + accuracy >= self.x >= other_right - accuracy \
                    and game_object.y <= self.y + self.height \
                    and game_object.y + game_object.height >= self.y:
                contacted.append(game_object)
        if Game.DEBUG and len(contacted):
            print("Objects collided left of object " + str(self) + ": " + str(contacted))
        return contacted

    # Get list of game objects touching the right side of this object
    def contact_right(self, accuracy=5):
        contacted = []
        # Iterate over all game objects
        for game_object in Game.GAME_OBJECTS:
            other_left = game_object.x
            # Lots of bounds checks
            if game_object != self \
                    and other_left + accuracy >= self.x + self.width >= other_left - accuracy \
                    and game_object.y <= self.y + self.height \
                    and game_object.y + game_object.height >= self.y:
                contacted.append(game_object)
        if Game.DEBUG and len(contacted):
            print("Objects collided right of object " + str(self) + ": " + str(contacted))
        return contacted

    # Get list of game objects touching the top of this object
    def contact_top(self, accuracy=5):
        contacted = []
        # Iterate over all game objects
        for game_object in Game.GAME_OBJECTS:
            other_bottom = game_object.y + game_object.height
            # Lots of bounds checks
            if game_object != self \
                    and other_bottom + accuracy >= self.y >= other_bottom - accuracy \
                    and game_object.x <= self.x + self.width \
                    and game_object.x + game_object.width >= self.x:
                contacted.append(game_object)
        if Game.DEBUG and len(contacted):
            print("Objects collided top of object " + str(self) + ": " + str(contacted))
        return contacted

    # Get list of game objects touching the bottom of this object
    def contact_bottom(self, accuracy=5):
        contacted = []
        # Iterate over all game objects
        for game_object in Game.GAME_OBJECTS:
            other_top = game_object.y
            # Lots of bounds checks
            if game_object != self \
                    and other_top + accuracy >= self.y + self.height >= other_top - accuracy \
                    and game_object.x <= self.x + self.width \
                    and game_object.x + game_object.width >= self.x:
                contacted.append(game_object)
        if Game.DEBUG and len(contacted):
            print("Objects collided bottom of object " + str(self) + ": " + str(contacted))
        return contacted

    # Remove this object from the game
    def destroy(self):
        try:
            Game.GAME_OBJECTS.remove(self)
            if Game.DEBUG:
                print("Destroy object: " + str(self))
        except ValueError:
            pass


# Class for player-controlled paddle
class Paddle(Renderable):
    def __init__(self, g: GraphWin):
        width = g.width / 4
        height = width * 0.15
        x = (g.width - width) / 2
        y = g.height - (height * 1.25)
        super().__init__(x=x, y=y, width=width, height=height, color="cyan")

    # Overridden update method
    def update(self, g: GraphWin, delta_t: float):
        # Start level when space is pressed
        if Game.GAME_STATE == 0 and Keyboard.is_key_triggered(key_symbols=["space"]):
            Game.GAME_STATE = 1
        if Game.GAME_STATE == 0 or Game.GAME_STATE == 1:
            # Move paddle left
            if self.x - self.height * 0.25 > 0 \
                    and Keyboard.is_key_pressed(key_symbols=["a", "Left"]):
                self.x = max(self.x - Game.BALL.base_speed / 1.5, self.height * 0.25)
            # Move paddle right
            if self.x + self.width + self.height * 0.25 < g.width \
                    and Keyboard.is_key_pressed(key_symbols=["d", "Right"]):
                self.x = min(self.x + Game.BALL.base_speed / 1.5, g.width - self.height * 0.25)
            # Force ball to follow paddle before level starts
            if Game.GAME_STATE == 0:
                Game.BALL.y = self.y - self.height * 0.25 - Game.BALL.height
                Game.BALL.x = self.x + (self.width - Game.BALL.width) / 2


# Font metrics
def get_string_width(size: float, text: str):
    scale = 30 / 36
    return (size * scale) * len(text)


# Font metrics
def get_string_height(size: float, text: str):
    scale = 43 / 36
    return (size * scale) * len(text.splitlines())


# Class for the game ball
class Ball(Renderable):
    def __init__(self, g: GraphWin):
        self.base_speed = 10
        width = g.width / 40
        self.d_x = (random.random() * 2 - 1) * (self.base_speed if random.randrange(0, 1) == 0 else -self.base_speed)
        self.d_y = -self.base_speed
        super().__init__(width=width, height=width)

    # Overridden update method
    def update(self, g: GraphWin, delta_t: float):
        if Game.GAME_STATE == 1:
            # Update position
            self.x += self.d_x
            self.y += self.d_y
            # Vertical bounds check
            if self.y <= 0:
                # Bounce off top
                self.d_y = abs(self.d_y)
            elif self.y > g.height:
                # Display lose screen if player is out of lives
                if Game.LIVES <= 0:
                    Game.GAME_STATE = 2
                    return
                # Decrement lives
                Game.LIVES -= 1
                # Reset ball & paddle
                Game.GAME_STATE = 0
                Game.PADDLE.x = (g.width - Game.PADDLE.width) / 2
                self.d_x = (random.random() * 2 - 1) * (self.base_speed if random.randrange(0, 1) == 0 else -self.base_speed)
                self.d_y = -abs(self.d_y)
                return
            # Horizontal bounds check
            if self.x <= 0:
                # Bounce off left
                self.d_x = abs(self.d_x)
                return
            elif self.x + self.width >= g.width:
                # Bounce off right
                self.d_x = -abs(self.d_x)
                return

            contacts = self.contact_right()
            for contacted in contacts:
                # Bounce
                self.d_x = -abs(self.d_x)
                # If the ball bounced off a brick, destroy the brick
                if isinstance(contacted, Brick):
                    contacted.destroy()
            contacts = self.contact_left()
            for contacted in contacts:
                # Bounce
                self.d_x = abs(self.d_x)
                # If the ball bounced off a brick, destroy the brick
                if isinstance(contacted, Brick):
                    contacted.destroy()
            contacts = self.contact_top()
            for contacted in contacts:
                # Bounce
                self.d_y = abs(self.d_y)
                # If the ball bounced off a brick, destroy the brick
                if isinstance(contacted, Brick):
                    contacted.destroy()
            contacts = self.contact_bottom()
            for contacted in contacts:
                # Bounce
                self.d_y = -abs(self.d_y)
                # If the ball bounced off a brick, destroy the brick
                if isinstance(contacted, Brick):
                    contacted.destroy()
                # If the ball bounced off the paddle, adjust the trajectory of the ball. This gives the player control over the direction of the ball
                elif isinstance(contacted, Paddle):
                    paddle_center = contacted.x + (contacted.width / 2)
                    center = self.x + (self.width / 2)
                    diff = paddle_center - center
                    if Game.DEBUG:
                        print("Paddle contact offset: " + str(diff))
                    diff = math.copysign(abs(diff) ** 1/16, diff) / 2
                    if Game.DEBUG:
                        print("Paddle contact modifier: " + str(diff))
                    self.d_x += diff
                    # Normalize the vector to not affect speed
                    length = math.sqrt(self.d_x ** 2 + self.d_y ** 2)
                    if length != 0:
                        self.d_x /= length / self.base_speed
                        self.d_y /= length / self.base_speed
        # Increment level and reset game objects if all bricks have been destroyed
        if len(Game.BRICKS) == 0:
            Game.GAME_STATE = 0
            Game.PADDLE.x = (g.width - Game.PADDLE.width) / 2
            Game.LEVEL += 1
            Game.BALL.base_speed += 0.5
            Game.gen_bricks(g)


# Class for the bricks
class Brick(Renderable):
    def __init__(self, g: GraphWin):
        width = g.width / 11
        height = width / 3
        super().__init__(width=width, height=height, color="white")

    # Overridden destroy method
    def destroy(self):
        Game.SCORE += 10
        # Grant an extra life
        if Game.SCORE % 250 == 0:
            Game.LIVES += 1
        # Update the high score
        if Game.SCORE > Game.HIGH_SCORE:
            Game.HIGH_SCORE = Game.SCORE
            Game.save_high_score()
        try:
            Game.BRICKS.remove(self)
        except ValueError:
            pass
        super().destroy()


# Class to keep information overlays in one place
class HUD:
    @staticmethod
    def draw(g: GraphWin):
        # Display high score
        string = "High Score: {}".format(Game.HIGH_SCORE)
        offset = get_string_width(20, string)
        txt = Text(Point(offset / 2, g.height - get_string_height(20, string)), string)
        txt.setTextColor("gray")
        txt.setFace("courier")
        txt.setSize(20)
        txt.draw(g)
        # Display current score
        string = "Score: {}".format(Game.SCORE)
        offset = get_string_width(20, string)
        txt = Text(Point(offset / 2, g.height - get_string_height(20, string) * 2.25), string)
        txt.setTextColor("gray")
        txt.setFace("courier")
        txt.setSize(20)
        txt.draw(g)
        # Display current level
        string = "Level: {}".format(Game.LEVEL)
        offset = get_string_width(20, string)
        txt = Text(Point(offset / 2, get_string_height(20, string)), string)
        txt.setTextColor("gray")
        txt.setFace("courier")
        txt.setSize(20)
        txt.draw(g)
        # Display current lives
        string = "Lives: {}".format(Game.LIVES)
        offset = get_string_width(20, string)
        txt = Text(Point(offset / 2, get_string_height(20, string) * 2.25), string)
        txt.setTextColor("gray")
        txt.setFace("courier")
        txt.setSize(20)
        txt.draw(g)
        # Display game state info
        if Game.PAUSED:
            txt = Text(Point(g.width / 2, g.height / 2), "PAUSED\n\"ESC\" TO RESUME")
            txt.setSize(36)
            txt.setFace("courier")
            txt.setTextColor("white")
            txt.draw(g)
        elif Game.GAME_STATE == 0:
            txt = Text(Point(g.width / 2, g.height / 2), "\"A/D/LEFT/RIGHT\" TO MOVE\n\"SPACE\" TO START")
            txt.setSize(36)
            txt.setFace("courier")
            txt.setTextColor("white")
            txt.draw(g)
        elif Game.GAME_STATE == 2:
            txt = Text(Point(g.width / 2, g.height / 2), "GAME OVER")
            txt.setSize(36)
            txt.setFace("courier")
            txt.setTextColor("white")
            txt.draw(g)


# Class to track keyboard key states
class KeyState:
    def __init__(self, key_code: int, key_symbol: str):
        self.key_code = key_code
        self.key_symbol = key_symbol
        self.pressed = False
        self.triggered = False

    def __repr__(self):
        return "[{}|'{}': triggered={} pressed={}]".format(self.key_code, self.key_symbol, self.triggered, self.pressed)


# Class to interface with a keyboard
class Keyboard:
    key_states = []

    @staticmethod
    def get_key_state(key_code: int=None, key_symbol: str=None):
        for state in Keyboard.key_states:
            if (key_code is not None and key_code == state.key_code)\
                    or (key_symbol is not None and key_symbol == state.key_symbol):
                if state.key_code is None and key_code is not None:
                    state.key_code = key_code
                if state.key_symbol is None and key_symbol is not None:
                    state.key_symbol = key_symbol
                return state
        state = KeyState(key_code, key_symbol)
        if Game.DEBUG:
            print("Created key state object: " + str(state))
        Keyboard.key_states.append(state)
        return state

    @staticmethod
    def update(*args):
        for state in Keyboard.key_states:
            was_triggered = state.triggered
            state.triggered = False
            if Game.DEBUG and was_triggered:
                print(str(state))

    @staticmethod
    def key_event(event: Event):
        event_type = int(event.type)
        key_code = event.keycode
        key_symbol = event.keysym
        if event_type == 2:
            # keypress
            state = Keyboard.get_key_state(key_code, key_symbol)
            if not state.pressed:
                state.triggered = True
            state.pressed = True
            if Game.DEBUG:
                print(str(state))
        elif event_type == 3:
            # keyrelease
            state = Keyboard.get_key_state(key_code, key_symbol)
            state.pressed = False
            if Game.DEBUG:
                print(str(state))

    @staticmethod
    def is_key_pressed(key_codes: int or List[int]=None, key_symbols: str or List[str]=None):
        if key_codes is None:
            if key_symbols is None:
                raise ValueError("Must provide key_codes or key_symbols")
            key_codes = []
        elif key_codes is int:
            key_codes = [key_codes]
        if key_symbols is None:
            key_symbols = []
        elif key_symbols is str:
            key_symbols = [key_symbols]
        for key_code in key_codes:
            if Keyboard.get_key_state(key_code=key_code).pressed:
                return True
        for key_symbol in key_symbols:
            if Keyboard.get_key_state(key_symbol=key_symbol).pressed:
                return True
        return False

    @staticmethod
    def is_key_triggered(key_codes: int or List[int]=None, key_symbols: str or List[str]=None):
        if key_codes is None:
            if key_symbols is None:
                raise ValueError("Must provide key_codes or key_symbols")
            key_codes = []
        elif key_codes is int:
            key_codes = [key_codes]
        if key_symbols is None:
            key_symbols = []
        elif key_symbols is str:
            key_symbols = [key_symbols]
        for key_code in key_codes:
            if Keyboard.get_key_state(key_code=key_code).triggered:
                return True
        for key_symbol in key_symbols:
            if Keyboard.get_key_state(key_symbol=key_symbol).triggered:
                return True
        return False


def update(g: GraphWin, delta_t: float):
    # Do not update objects if game is paused
    if not Game.PAUSED:
        # Update game objects
        for obj in Game.GAME_OBJECTS:
            obj.update(g, delta_t)
    # Toggle paused state
    if Keyboard.is_key_triggered(key_symbols=["Escape"]):
        Game.PAUSED = not Game.PAUSED
    # Update the keyboard, necessary to clear trigger flags
    Keyboard.update()


# Global draw method
def draw(g: GraphWin):
    # Clear the window
    g.delete("all")
    # Draw game objects
    for obj in Game.GAME_OBJECTS:
        obj.draw(g)
    # Draw the HUD
    HUD.draw(g)
    # Trigger a window redraw
    g.update()


def main():
    # Create the graphics window
    g = GraphWin("Breakout", 800, 600, autoflush=False)
    g.setBackground("black")

    # Load the high score
    Game.load_high_score()

    # Create initial game objects
    Game.PADDLE = Paddle(g)
    Game.GAME_OBJECTS.append(Game.PADDLE)
    Game.BALL = Ball(g)
    Game.GAME_OBJECTS.append(Game.BALL)
    Game.gen_bricks(g)

    # Update on separate thread
    game_timer = RepeatingTimer(target=update, args=[g, Game.FRAMERATE], period=Game.FRAMERATE, daemon=True)
    game_timer.start()

    # Pass keyboard events to Keyboard
    g.bind_all("<KeyPress>", Keyboard.key_event, True)
    g.bind_all("<KeyRelease>", Keyboard.key_event, True)

    # Draw loop on main thread because tkinter doesn't like multithreading
    while not g.isClosed():
        now = datetime.now()
        draw(g)
        elapsed = (datetime.now() - now).microseconds
        if elapsed < Game.FRAMERATE:
            time.sleep(Game.FRAMERATE - (elapsed * 0.000001))
    # Stop the update thread and quit
    game_timer.cancel()
    exit()

main()
